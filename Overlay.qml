import QtQuick 2.0
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    //type is one of "underconstruction", "before" or "after"
    property alias type: svg.elementId

    //if we need to expand to have overlays in many places
    //this can become a list of components + a loader putting things in many places

    PlasmaCore.SvgItem {
        id: svg
        svg: PlasmaCore.Svg {
                imagePath: Qt.resolvedUrl("assets/overlays.svgz")
            }
        elementId: "underconstruction"

        width: 200
        height: 200
        z: 1000

        anchors {
            top: parent.top
            topMargin: mode == "underconstruction" ? -29 : -56
            right: parent.right
            rightMargin: mode == "underconstruction" ? -24 : -68
        }
    }


}