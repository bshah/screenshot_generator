import QtQuick 2.0

Item {
    clip: true

    property alias imageSource: image.source
    property alias overlayType: overlay.type

    Flickable {
        id: content1
        anchors.fill: parent

        contentWidth: image.width; contentHeight: image.height

        Image {
            id: image
            //image size is left at it's natural size
        }

    }
    Overlay {
        id: overlay
        anchors.fill: content1
    }
}